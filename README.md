# Arnie

Arnie is a Matrix bot.

## Development

To set up:

* [Install rust](https://www.rust-lang.org/tools/install)
* Install prerequisites:
```sh
rustup component add rustfmt
```

Then, to compile and run tests:

```sh
cargo test
```

Or to run:

(Temporarily we have to poke config into Redis directly.)

```sh
redis-cli
SET "arniebot|matrix_sender_receiver|homeserver_url" "http://localhost:8008"
SET "arniebot|matrix_sender_receiver|username" "myusername"
SET "arniebot|matrix_sender_receiver|password" "mypassword"
```

```sh
$ ARNIE_DB_PREFIX="arniebot|" ARNIE_REDIS_URL="redis://localhost:6379" cargo run
>>> !arnie
Get out.
```

## License

Copyright 2019-2023 Andy Balaam and the Arnie contributors.

Released under the AGPLv3 license. See [LICENSE](LICENSE) for info.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
