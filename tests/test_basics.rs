use std::io;
use std::sync::Arc;
use tokio::sync::Mutex;

use arnie::bot::Bot;
use arnie::command_line_receiver::CommandLineReceiver;
use arnie::command_line_sender::CommandLineSender;
use arnie::database_wrapper::DatabaseWrapper;
use arnie::error_stream::ErrorStream;
use arnie::memory_database::MemoryDatabase;
use arnie::write_buffer::WriteBuffer;

#[tokio::test]
async fn can_start_and_stop() {
    let (stdout, _stderr) = run_bot("exit\n").await;
    assert_eq!(stdout, ">>> Exiting\n>>> ");
}

#[tokio::test]
async fn default_plugin_is_arnie() {
    let (stdout, _stderr) = run_bot("!arnie\nexit\n").await;
    assert_eq!(
        stdout,
        "\
        >>> You got what you want Cohagen, give dese people air!\n\
        >>> Exiting\n\
        >>> "
    );
}

#[tokio::test]
async fn print_error() {
    let (stdout, stderr) = run_bot("!print_error foo\nexit\n").await;
    assert_eq!(
        stdout,
        "\
        >>> \n\
        >>> Exiting\n\
        >>> "
    );
    // TODO: arnie runs even though this hit another plugin
    assert_eq!(stderr, "system_admin_plugin: foo\n");
}

async fn run_bot(stdin: &str) -> (String, String) {
    let stdin = io::Cursor::new(stdin.to_owned());
    let stdout = WriteBuffer::new();
    let stderr = WriteBuffer::new();

    let mem_db = DatabaseWrapper::new(
        Arc::new(Mutex::new(MemoryDatabase::new())),
        String::from(""),
    );
    let mut bot = Bot::new(mem_db, ErrorStream::new(stderr.clone()), 1)
        .await
        .expect("Failed to create bot");
    bot.add_message_receiver(CommandLineReceiver::new(stdin));
    bot.add_message_sender(Box::new(
        CommandLineSender::new(stdout.clone()).unwrap(),
    ));
    match bot.run().await {
        Ok(_) => {
            panic!("Should returnexit code")
        }
        Err(code) => {
            assert_eq!(code, 0)
        }
    }

    (stdout.to_string(), stderr.to_string())
}
