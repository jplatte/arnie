use async_trait::async_trait;
use tokio::io::{AsyncBufReadExt, AsyncRead, BufReader};

use crate::message::Message;
use crate::message_receiver::MessageReceiver;
use crate::room_address::RoomAddress;

pub struct CommandLineReceiver<In: AsyncRead + Send + Unpin> {
    stdin: BufReader<In>,
}

impl<In: AsyncRead + Send + Unpin> CommandLineReceiver<In> {
    pub fn new(stdin: In) -> Self {
        Self {
            stdin: BufReader::new(stdin),
        }
    }
}

#[async_trait]
impl<In: AsyncRead + Send + Unpin> MessageReceiver for CommandLineReceiver<In> {
    async fn into_callback_per_message(
        self,
        callback: Box<dyn Fn(Message) + Send + Sync>,
    ) {
        let mut lines = self.stdin.lines();
        loop {
            let line = lines.next_line().await;
            match line {
                Err(_e) => {
                    // TODO: print error
                    break;
                }
                Ok(None) => {
                    // TODO: print message
                    break;
                }
                Ok(Some(line)) => {
                    callback(Message::new(line, RoomAddress::CommandLine))
                }
            }
        }
    }
}
