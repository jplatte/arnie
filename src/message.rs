use crate::room_address::RoomAddress;

#[derive(Debug, PartialEq)]
pub struct Message {
    pub text: String,
    pub room_address: RoomAddress,
}

impl Message {
    pub fn new(text: String, room_address: RoomAddress) -> Self {
        Self { text, room_address }
    }
}
