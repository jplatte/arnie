use async_trait::async_trait;
use redis::{RedisError, RedisResult};
use std::collections::HashMap;

use crate::database::Database;

/**
 * A very simple Redis-like in-memory DB.
 *
 * Behaviour is undefined if you insert values of one type and then extract
 * them as if they were another.
 */
#[derive(Default)]
pub struct MemoryDatabase {
    map: HashMap<String, String>,
}

impl MemoryDatabase {
    pub fn new() -> MemoryDatabase {
        MemoryDatabase {
            map: HashMap::new(),
        }
    }

    fn lookup_vec(&self, key: &str) -> Result<Vec<String>, RedisError> {
        let val = self.map.get(key);
        match val {
            None => Ok(Vec::new()),
            Some(val) => {
                serde_json::from_str::<Vec<String>>(val).map_err(|_| {
                    RedisError::from((
                        redis::ErrorKind::TypeError,
                        "Treating a non-list as a list",
                    ))
                })
            }
        }
    }

    fn insert_vec(&mut self, key: &str, vec: Vec<String>) {
        self.map.insert(
            String::from(key),
            serde_json::to_string(&vec).expect("Vec failed to serialise!"),
        );
    }
}

fn filter_n<T, C>(
    items: impl Iterator<Item = T>,
    to_remove: &C,
    count: usize,
) -> Vec<T>
where
    T: PartialEq<C>,
    C: ?Sized,
{
    let mut c = count;
    items
        .filter(|v| {
            if c == 0 {
                true
            } else if v == to_remove {
                c -= 1;
                false
            } else {
                true
            }
        })
        .collect()
}

#[async_trait]
impl Database for MemoryDatabase {
    async fn get(&mut self, key: &str) -> RedisResult<Option<String>> {
        Ok(self.map.get(key).cloned())
    }

    async fn exists(&mut self, key: &str) -> RedisResult<bool> {
        Ok(self.map.contains_key(key))
    }

    async fn set(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.map.insert(key.to_owned(), value.to_owned());
        Ok(())
    }

    async fn set_usize(&mut self, key: &str, value: usize) -> RedisResult<()> {
        self.map.insert(key.to_owned(), value.to_string());
        Ok(())
    }

    async fn lpush(&mut self, key: &str, value: &str) -> RedisResult<()> {
        let existing = self.map.get(key);
        let mut existing: Vec<String> = if let Some(e) = existing {
            serde_json::from_str::<Vec<String>>(e).map_err(|_| {
                RedisError::from((
                    redis::ErrorKind::TypeError,
                    "Trying to lpush onto something that is not a list",
                ))
            })?
        } else {
            Vec::new()
        };
        existing.push(value.to_owned());
        self.insert_vec(key, existing);
        Ok(())
    }

    async fn llen(&mut self, key: &str) -> RedisResult<usize> {
        self.lookup_vec(key).map(|v| v.len())
    }

    async fn lindex(
        &mut self,
        key: &str,
        index: isize,
    ) -> RedisResult<Option<String>> {
        // Redis treats negative indices as their equivalent positive
        let index = index.unsigned_abs();

        Ok(self.lookup_vec(key)?.get(index).cloned())
    }

    async fn lrem(
        &mut self,
        key: &str,
        count: isize,
        value: &str,
    ) -> RedisResult<()> {
        let v = self.lookup_vec(key)?;

        // TODO: don't actually need to clone here
        let v = match count.cmp(&0) {
            std::cmp::Ordering::Less => {
                let mut filtered =
                    filter_n(v.into_iter().rev(), value, -count as usize);
                filtered.reverse();
                filtered
            }
            std::cmp::Ordering::Equal => {
                v.iter().filter(|s| *s != value).cloned().collect()
            }
            std::cmp::Ordering::Greater => {
                filter_n(v.into_iter(), value, count as usize)
            }
        };

        self.insert_vec(key, v);
        Ok(())
    }

    async fn lset(
        &mut self,
        key: &str,
        index: isize,
        value: &str,
    ) -> RedisResult<()> {
        // Redis treats negative indices as their equivalent positive
        let index = index.unsigned_abs();
        let mut v = self.lookup_vec(key)?;
        v[index] = value.to_owned();
        self.insert_vec(key, v);
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use redis::RedisError;

    use super::*;

    #[test]
    fn filter_n_returns_empty_vec_for_empty_iterator() {
        let v: Vec<String> = Vec::new();
        let filtered = filter_n(v.clone().into_iter(), "removeme", 10);
        assert_eq!(filtered, v);
    }

    #[test]
    fn filter_n_removes_all_matching_elements_if_count_is_high() {
        let v = vec!["one", "two", "two", "two", "three"];
        let filtered = filter_n(v.into_iter(), &"two", 10);
        assert_eq!(filtered, vec!["one", "three"]);
    }

    #[test]
    fn filter_n_removes_some_matching_elements() {
        let v = vec!["one", "two", "two", "two", "three"];
        let filtered = filter_n(v.into_iter(), &"two", 2);
        assert_eq!(filtered, vec!["one", "two", "three"]);
    }

    #[test]
    fn filter_n_removes_elements_from_beginning_and_end() {
        let v = vec!["two", "one", "two", "three", "two"];
        let filtered = filter_n(v.into_iter(), &"two", 3);
        assert_eq!(filtered, vec!["one", "three"]);
    }

    #[test]
    fn filter_n_removes_no_elements_if_count_is_zero() {
        let v = vec!["one", "two", "two", "two", "three"];
        let filtered = filter_n(v.clone().into_iter(), &"two", 0);
        assert_eq!(filtered, v);
    }

    #[tokio::test]
    async fn returns_none_for_unused_key() {
        let mut db = MemoryDatabase::new();
        assert_eq!(db.get("k1").await.unwrap(), None);
    }

    #[tokio::test]
    async fn can_store_and_retrieve_strings() {
        let mut db = MemoryDatabase::new();
        db.set("k1", "v1").await.unwrap();
        assert_eq!(db.get("k1").await.unwrap(), Some("v1".to_owned()));
    }

    #[tokio::test]
    async fn exists_says_whether_key_is_used() {
        let mut db = MemoryDatabase::new();
        assert!(!db.exists("k1").await.unwrap());
        db.set("k1", "v1").await.unwrap();
        assert!(db.exists("k1").await.unwrap());
    }

    #[tokio::test]
    async fn exists_says_whether_key_is_used_for_usize() {
        let mut db = MemoryDatabase::new();
        assert!(!db.exists("k1").await.unwrap());
        db.set_usize("k1", 3).await.unwrap();
        assert!(db.exists("k1").await.unwrap());
    }

    #[tokio::test]
    async fn can_store_a_usize_and_check_key_exists() {
        let mut db = MemoryDatabase::new();
        db.set_usize("ku", 3).await.unwrap();
        // Currently we don't support get_usize. Presumably,
        // soon, we will, and we should update this to use get_usize.
        assert!(db.exists("ku").await.unwrap());
    }

    #[tokio::test]
    async fn can_push_a_value_into_an_unused_key_and_retrieve_it() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "item").await.unwrap();
        assert_eq!(db.lindex("ll", 0).await.unwrap(), Some("item".to_owned()));
    }

    #[tokio::test]
    async fn treating_a_string_as_list_when_pushing_is_an_error() {
        let mut db = MemoryDatabase::new();
        db.set("ll", "value").await.unwrap();
        assert_eq!(
            db.lpush("ll", "item").await,
            Err(RedisError::from((
                redis::ErrorKind::TypeError,
                "Trying to lpush onto something that is not a list"
            )))
        );
    }

    #[tokio::test]
    async fn treating_a_string_as_list_when_indexing_is_an_error() {
        let mut db = MemoryDatabase::new();
        db.set("ll", "value").await.unwrap();
        assert_eq!(
            db.lindex("ll", 3).await,
            Err(RedisError::from((
                redis::ErrorKind::TypeError,
                "Trying to lindex into something that is not a list"
            )))
        );
    }

    #[tokio::test]
    async fn can_retrieve_list_values_by_index() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "itemZ1").await.unwrap();
        db.lpush("ll", "itemA2").await.unwrap();
        db.lpush("ll", "itemX3").await.unwrap();
        db.lpush("ll", "itemW4").await.unwrap();
        assert_eq!(
            db.lindex("ll", 3).await.unwrap(),
            Some("itemW4".to_owned())
        );
        assert_eq!(
            db.lindex("ll", 2).await.unwrap(),
            Some("itemX3".to_owned())
        );
        assert_eq!(
            db.lindex("ll", 0).await.unwrap(),
            Some("itemZ1".to_owned())
        );
        assert_eq!(
            db.lindex("ll", 1).await.unwrap(),
            Some("itemA2".to_owned())
        );
    }

    #[tokio::test]
    async fn retrieving_list_items_off_the_end_returns_none() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "itemZ1").await.unwrap();
        db.lpush("ll", "itemA2").await.unwrap();
        assert_eq!(db.lindex("ll", 2).await.unwrap(), None);
    }

    #[tokio::test]
    async fn retrieving_list_items_using_negative_indices_gets_positive() {
        // This is what Redis does: ours is not to reason why
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "itemZ0").await.unwrap();
        db.lpush("ll", "itemA1").await.unwrap();
        db.lpush("ll", "itemA2").await.unwrap();
        assert_eq!(
            db.lindex("ll", -2).await.unwrap(),
            Some("itemA2".to_owned())
        );
    }

    #[tokio::test]
    async fn can_find_the_length_of_a_list() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "itemZ1").await.unwrap();
        db.lpush("ll", "itemA2").await.unwrap();
        db.lpush("ll", "itemX3").await.unwrap();
        assert_eq!(db.llen("ll").await.unwrap(), 3);
    }

    #[tokio::test]
    async fn length_of_an_unused_key_is_zero() {
        let mut db = MemoryDatabase::new();
        assert_eq!(db.llen("ll").await.unwrap(), 0);
    }

    #[tokio::test]
    async fn asking_llen_of_a_string_is_an_error() {
        let mut db = MemoryDatabase::new();
        db.set("ll", "value").await.unwrap();
        assert_eq!(
            db.llen("ll").await,
            Err(RedisError::from((
                redis::ErrorKind::TypeError,
                "Trying to find the list length of something that is not a list"
            )))
        );
    }

    #[tokio::test]
    async fn can_set_items_in_a_list() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "itemZ1").await.unwrap();
        db.lpush("ll", "itemA2").await.unwrap();
        db.lpush("ll", "itemX3").await.unwrap();
        db.lpush("ll", "itemW4").await.unwrap();

        // This is what we are testing: set an item
        db.lset("ll", 2, "CHANGED").await.unwrap();

        // The item we changed was updated
        assert_eq!(
            db.lindex("ll", 2).await.unwrap(),
            Some("CHANGED".to_owned())
        );

        // Other items are unchanged
        assert_eq!(
            db.lindex("ll", 3).await.unwrap(),
            Some("itemW4".to_owned())
        );
    }

    #[tokio::test]
    async fn can_remove_n_matching_items_from_a_list() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "item1").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item3").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item6").await.unwrap();

        // This is what we are testing: delete the first 2
        db.lrem("ll", 2, "deleteme").await.unwrap();

        // 2 were deleted
        assert_eq!(db.llen("ll").await.unwrap(), 4);
        assert_eq!(db.lindex("ll", 0).await.unwrap().unwrap(), "item1");
        assert_eq!(db.lindex("ll", 1).await.unwrap().unwrap(), "item3");
        assert_eq!(db.lindex("ll", 2).await.unwrap().unwrap(), "deleteme");
        assert_eq!(db.lindex("ll", 3).await.unwrap().unwrap(), "item6");
    }

    #[tokio::test]
    async fn can_remove_n_matching_items_from_the_end_of_a_list() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "item1").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item3").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item6").await.unwrap();

        // This is what we are testing: delete the last 1
        db.lrem("ll", -2, "deleteme").await.unwrap();

        // It was deleted
        assert_eq!(db.llen("ll").await.unwrap(), 4);
        assert_eq!(db.lindex("ll", 0).await.unwrap().unwrap(), "item1");
        assert_eq!(db.lindex("ll", 1).await.unwrap().unwrap(), "deleteme");
        assert_eq!(db.lindex("ll", 2).await.unwrap().unwrap(), "item3");
        assert_eq!(db.lindex("ll", 3).await.unwrap().unwrap(), "item6");
    }

    #[tokio::test]
    async fn can_remove_last_matching_item_from_the_end_of_a_list() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "item1").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item3").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item6").await.unwrap();

        // This is what we are testing: delete the last 1
        db.lrem("ll", -1, "deleteme").await.unwrap();

        // It was deleted
        assert_eq!(db.llen("ll").await.unwrap(), 5);
        assert_eq!(db.lindex("ll", 0).await.unwrap().unwrap(), "item1");
        assert_eq!(db.lindex("ll", 1).await.unwrap().unwrap(), "deleteme");
        assert_eq!(db.lindex("ll", 2).await.unwrap().unwrap(), "item3");
        assert_eq!(db.lindex("ll", 3).await.unwrap().unwrap(), "deleteme");
        assert_eq!(db.lindex("ll", 4).await.unwrap().unwrap(), "item6");
    }

    #[tokio::test]
    async fn can_remove_all_matching_items_from_a_list() {
        let mut db = MemoryDatabase::new();
        db.lpush("ll", "item1").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item3").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "deleteme").await.unwrap();
        db.lpush("ll", "item6").await.unwrap();

        // This is what we are testing: delete all
        db.lrem("ll", 0, "deleteme").await.unwrap();

        // 2 were deleted
        assert_eq!(db.llen("ll").await.unwrap(), 3);
        assert_eq!(db.lindex("ll", 0).await.unwrap().unwrap(), "item1");
        assert_eq!(db.lindex("ll", 1).await.unwrap().unwrap(), "item3");
        assert_eq!(db.lindex("ll", 2).await.unwrap().unwrap(), "item6");
    }
}
