use clap::Parser;

#[derive(Debug, Parser)]
#[clap(
    version = env!("CARGO_PKG_VERSION"),
    author = env!("CARGO_PKG_AUTHORS"),
    name = env!("CARGO_PKG_NAME"),
    about = env!("CARGO_PKG_DESCRIPTION"),
)]
pub struct Args {
    #[clap(short, long, env = "ARNIE_REDIS_URL")]
    pub redis_url: Option<String>,

    #[clap(short, long, default_value = "", env = "ARNIE_DB_PREFIX")]
    pub db_prefix: String,
}
