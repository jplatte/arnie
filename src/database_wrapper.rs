use redis::RedisResult;
use std::sync::Arc;
use tokio::sync::Mutex;

use crate::database::Database;

pub struct DatabaseWrapper<Db: Database> {
    db: Arc<Mutex<Db>>,
    prefix: String,
}

impl<Db: Database> DatabaseWrapper<Db> {
    pub fn new(db: Arc<Mutex<Db>>, prefix: String) -> Self {
        Self { db, prefix }
    }

    pub fn namespaced(&self, extra_prefix: &str) -> Self {
        let p = self.prefix.clone() + extra_prefix;
        Self::new(self.db.clone(), p)
    }
}

impl<Db: Database> DatabaseWrapper<Db> {
    pub async fn get<RV>(&mut self, key: &str) -> RedisResult<Option<String>> {
        self.db.lock().await.get(&self.key(key)).await
    }

    pub async fn exists(&mut self, key: &str) -> RedisResult<bool> where {
        self.db.lock().await.exists(&self.key(key)).await
    }

    pub async fn set(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.db.lock().await.set(&self.key(key), value).await
    }

    pub async fn set_usize(
        &mut self,
        key: &str,
        value: usize,
    ) -> RedisResult<()> {
        self.db.lock().await.set_usize(&self.key(key), value).await
    }

    pub async fn lpush(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.db.lock().await.lpush(&self.key(key), value).await
    }

    pub async fn lrem(
        &mut self,
        key: &str,
        count: isize,
        value: &str,
    ) -> RedisResult<()> {
        self.db
            .lock()
            .await
            .lrem(&self.key(key), count, value)
            .await
    }

    pub async fn lset(
        &mut self,
        key: &str,
        index: isize,
        value: &str,
    ) -> RedisResult<()> {
        self.db
            .lock()
            .await
            .lset(&self.key(key), index, value)
            .await
    }

    pub async fn llen(&self, key: &str) -> RedisResult<usize> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.llen(&k);
        f.await
    }

    pub async fn lindex(
        &self,
        key: &str,
        index: isize,
    ) -> RedisResult<Option<String>> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.lindex(&k, index);
        f.await
    }

    fn key(&self, k: &str) -> String {
        self.prefix.clone() + k
    }
}

#[cfg(test)]
mod tests {
    // TODO tests
    #[test]
    fn x() {
        assert_eq!(2, 2);
    }
}
