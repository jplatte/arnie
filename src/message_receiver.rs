use async_trait::async_trait;

use crate::message::Message;

/**
 * Something that can receive messages and call a callback for
 * each one it receives.
 */
#[async_trait]
pub trait MessageReceiver {
    /**
     * Receive messages, calling callback with each message.  Consumes this
     * receiver.
     *
     * This method should only return when an error means that this receiver
     * is no longer usable.
     */
    async fn into_callback_per_message(
        self,
        callback: Box<dyn Fn(Message) + Send + Sync>,
    );
}
