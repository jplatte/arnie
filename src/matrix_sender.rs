use async_trait::async_trait;
use matrix_sdk::config::SyncSettings;
use matrix_sdk::ruma::events::room::message::RoomMessageEventContent;
use matrix_sdk::ruma::{OwnedRoomId, RoomId};
use matrix_sdk::{Client, ClientBuildError};
use maud::html;

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::message::Message;
use crate::message_sender::{MessageSender, SendError, WrongRoomAddressType};
use crate::room_address::RoomAddress;

pub struct MatrixSender<Db: Database> {
    client: Client,
    _db: DatabaseWrapper<Db>,
}

impl<Db: Database> MatrixSender<Db> {
    pub async fn new(
        mut db: DatabaseWrapper<Db>,
    ) -> Result<Self, ClientBuildError> {
        // TODO: return an error when there is a missing entry in the DB.
        // TODO: later: start up fine even if missing entry and poll
        //       for good DB entries?
        let homeserver_url = db
            .get::<Option<String>>("homeserver_url")
            .await
            .expect("Error fetching homeserver_url")
            .expect("No homeserver_url!");
        let username = db
            .get::<Option<String>>("username")
            .await
            .expect("Error fetching username")
            .expect("No username!");
        let password = db
            .get::<Option<String>>("password")
            .await
            .expect("Error fetching password")
            .expect("No password!");

        // TODO: use a consistent device ID and store crypto keys
        //let device_id = Some("andysdevice3");

        let client = Client::builder()
            .homeserver_url(homeserver_url)
            .build()
            .await?;

        let login = client.login_username(username, &password);
        /*if let Some(device_id) = &device_id {
            login = login.device_id(device_id);
        }*/
        login.initial_device_display_name("arnie").await.unwrap();

        // TODO: spawn a task that keeps syncing
        client.sync_once(SyncSettings::default()).await.unwrap();

        println!("arnie: connected to matrix sender");

        Ok(Self { _db: db, client })
    }
}

fn find_room_id(message: &Message) -> Result<OwnedRoomId, SendError> {
    if let RoomAddress::Matrix(rid) = &message.room_address {
        RoomId::parse(rid)
            .map_err(|_| SendError::new(Box::new(WrongRoomAddressType {})))
    } else {
        Err(SendError::new(Box::new(WrongRoomAddressType {})))
    }
}

#[async_trait]
impl<Db: Database + Send> MessageSender for MatrixSender<Db> {
    // TODO: unwrap
    async fn send(&mut self, message: &Message) -> Result<(), SendError> {
        let rid = find_room_id(message)?;
        if let Some(room) = self.client.get_joined_room(&rid) {
            let text_body = format!("**{}**", message.text);
            let html_body = html! { strong { (message.text) } };
            let content =
                RoomMessageEventContent::text_html(&text_body, html_body);
            room.send(content, None).await.expect("Failed to send!");
        }

        Ok(())
    }
}
