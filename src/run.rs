use redis::RedisResult;
use std::io::Write;
use std::sync::Arc;
use tokio::io::AsyncRead;
use tokio::sync::Mutex;

use crate::args::Args;
use crate::bot::Bot;
use crate::command_line_receiver::CommandLineReceiver;
use crate::command_line_sender::CommandLineSender;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::error_stream::ErrorStream;
use crate::matrix_receiver::MatrixReceiver;
use crate::matrix_sender::MatrixSender;
use crate::memory_database::MemoryDatabase;
use crate::redis_database::RedisDatabase;

pub async fn run<
    In: 'static + AsyncRead + Send + Unpin,
    Ou: 'static + Write + Send,
    Er: 'static + Write,
>(
    args: Args,
    stdin: In,
    stdout: Ou,
    stderr: Er,
    random_seed: u64,
) -> RedisResult<i32> {
    let redis_url = args.redis_url.clone();
    if let Some(redis_url) = redis_url {
        let redis_db =
            Arc::new(Mutex::new(RedisDatabase::new(redis_url).await?));
        launch(args, stdin, stdout, stderr, redis_db, random_seed).await
    } else {
        let mem_db = Arc::new(Mutex::new(MemoryDatabase::new()));
        launch(args, stdin, stdout, stderr, mem_db, random_seed).await
    }
}

async fn launch<
    Db: 'static + Database + Send,
    In: 'static + AsyncRead + Send + Unpin,
    Ou: 'static + Write + Send,
    Er: 'static + Write,
>(
    args: Args,
    stdin: In,
    stdout: Ou,
    stderr: Er,
    db: Arc<Mutex<Db>>,
    random_seed: u64,
) -> RedisResult<i32> {
    let errors = ErrorStream::new(stderr);
    let wrapped_db = DatabaseWrapper::new(db, args.db_prefix);
    let matrix_sender_db = wrapped_db.namespaced("matrix_sender_receiver|");
    let matrix_receiver_db = wrapped_db.namespaced("matrix_sender_receiver|");
    let mut bot = Bot::new(wrapped_db, errors, random_seed).await?;
    bot.add_message_sender(Box::new(CommandLineSender::new(stdout)?));
    bot.add_message_sender(Box::new(
        MatrixSender::new(matrix_sender_db)
            .await
            .expect("Failed to make Matrix Sender"),
    ));
    bot.add_message_receiver(CommandLineReceiver::new(stdin));
    bot.add_message_receiver(
        MatrixReceiver::new(matrix_receiver_db)
            .await
            .expect("Failed to make Matrix Receiver"), // TODO: unwrap
    );
    match bot.run().await {
        Ok(()) => Ok(0),
        Err(code) => Ok(code),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::write_buffer::WriteBuffer;
    use clap::Parser;

    #[tokio::test]
    #[ignore]
    async fn can_start_and_stop_bot() {
        let args = Args::parse_from([""]);
        let stdin = "exit\n".as_bytes();
        let stdout = WriteBuffer::new();
        let stderr = WriteBuffer::new();
        run(args, stdin, stdout.clone(), stderr.clone(), 2)
            .await
            .expect("Should succeed.");

        assert_eq!(stdout.to_string(), ">>> Exiting\n>>> ");
        assert_eq!(stderr.to_string(), "");
    }
}
