use async_trait::async_trait;
use std::io::{self, Write};

use crate::message::Message;
use crate::message_sender::{MessageSender, SendError};

pub struct CommandLineSender<Ou: Write> {
    stdout: Ou,
}

impl<Ou: Write + Send> CommandLineSender<Ou> {
    pub fn new(stdout: Ou) -> Result<Self, io::Error> {
        let mut ret = Self { stdout };
        ret.prompt()?;
        Ok(ret)
    }

    fn prompt(&mut self) -> Result<(), io::Error> {
        write!(self.stdout, ">>> ")?;
        self.stdout.flush()?;
        Ok(())
    }
}

#[async_trait]
impl<Ou: Write + Send> MessageSender for CommandLineSender<Ou> {
    async fn send(&mut self, message: &Message) -> Result<(), SendError> {
        writeln!(self.stdout, "{}", message.text)?;
        self.prompt()?;
        Ok(())
    }
}
