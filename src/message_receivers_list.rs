use tokio::sync::mpsc;
use tokio::time::{sleep, Duration};

use crate::error_stream::ErrorStream;
use crate::message::Message;
use crate::message_receiver::MessageReceiver;

const MESSAGE_QUEUE_SIZE: usize = 10000;

/**
 * Holds open multiple MessageReceivers, and provides
 * next_message() to access the messages coming in on them.
 */
pub struct MessageReceiversList {
    sender: mpsc::Sender<Message>,
    receiver: mpsc::Receiver<Message>,
    errors: ErrorStream,
}

impl MessageReceiversList {
    pub fn new(errors: ErrorStream) -> Self {
        let (sender, receiver) = mpsc::channel(MESSAGE_QUEUE_SIZE);
        Self {
            sender,
            receiver,
            errors,
        }
    }

    /**
     * Provide a MessageReceiver and collect its messages, making
     * them available to anyone who calls next_message on this
     * MessageReceiversList.
     */
    pub fn add<M: MessageReceiver + 'static>(&mut self, receiver: M) {
        let sender = self.sender.clone();
        let f = receiver.into_callback_per_message(Box::new(move |message| {
            sender.try_send(message).expect("Unable to send!");
        }));
        tokio::task::spawn(f);
    }

    /**
     * Get the next message provided by one of the MessageReceivers
     * that were added via the add method.
     */
    pub async fn next_message(&mut self) -> Message {
        loop {
            if let Some(message) = self.receiver.recv().await {
                return message;
            } else {
                self.errors.send(
                    "MessageReceiversList",
                    "Receiving messages returned None.",
                );
                sleep(Duration::from_secs(1)).await;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use async_trait::async_trait;
    use tokio::time::{sleep, Duration};

    use super::*;
    use crate::message::Message;
    use crate::room_address::RoomAddress;
    use crate::write_buffer::WriteBuffer;

    #[tokio::test]
    async fn can_get_a_message_from_a_single_messenger() {
        let stderr = WriteBuffer::new();
        let messenger = FakeMessageReceiver::new(vec![String::from("mes1")]);
        let mut list = MessageReceiversList::new(ErrorStream::new(stderr));
        list.add(messenger);
        let msg = list.next_message().await;
        assert_eq!(
            msg,
            Message::new("mes1".to_owned(), RoomAddress::CommandLine)
        );
    }

    struct FakeMessageReceiver {
        msgs: Vec<String>,
    }

    impl FakeMessageReceiver {
        fn new(msgs: Vec<String>) -> Self {
            Self { msgs }
        }
    }

    #[async_trait]
    impl MessageReceiver for FakeMessageReceiver {
        async fn into_callback_per_message(
            mut self,
            f: Box<dyn Fn(Message) -> () + Send + Sync>,
        ) {
            while self.msgs.len() > 0 {
                sleep(Duration::from_millis(1)).await;
                f(Message::new(self.msgs.remove(0), RoomAddress::CommandLine));
            }
        }
    }
}
