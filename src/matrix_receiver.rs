use std::time::Duration;

use async_trait::async_trait;
use matrix_sdk::config::SyncSettings;
use matrix_sdk::room::Room;
use matrix_sdk::ruma::events::room::message::{
    MessageType, OriginalSyncRoomMessageEvent,
};
use matrix_sdk::{Client, ClientBuildError};
use tokio::time::{sleep, Instant};

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::message::Message;
use crate::message_receiver::MessageReceiver;
use crate::room_address::RoomAddress;

/// We refuse to sync again until at least this long since the last time
/// we synced.
const MIN_TIME_BETWEEN_SYNCS: Duration = Duration::from_secs(30);

pub struct MatrixReceiver<Db: Database> {
    client: Client,
    _db: DatabaseWrapper<Db>,
}

impl<Db: Database> MatrixReceiver<Db> {
    pub async fn new(
        mut db: DatabaseWrapper<Db>,
    ) -> Result<Self, ClientBuildError> {
        // TODO: return an error when there is a missing entry in the DB.
        // TODO: later: start up fine even if missing entry and poll
        //       for good DB entries?
        let homeserver_url = db
            .get::<Option<String>>("homeserver_url")
            .await
            .expect("Error fetching homeserver_url")
            .expect("No homeserver_url!");
        let username = db
            .get::<Option<String>>("username")
            .await
            .expect("Error fetching username")
            .expect("No username!");
        let password = db
            .get::<Option<String>>("password")
            .await
            .expect("Error fetching password")
            .expect("No password!");

        //let device_id = Some("andysdevice3");

        let client = Client::builder()
            .homeserver_url(homeserver_url)
            .build()
            .await?;

        let login = client.login_username(username, &password);
        /*if let Some(device_id) = &device_id {
            login = login.device_id(device_id);
        }*/
        login.initial_device_display_name("arnie").await.unwrap();
        println!("arnie: connected to matrix receiver");

        Ok(Self { _db: db, client })
    }
}

#[async_trait]
impl<Db: Database + Send> MessageReceiver for MatrixReceiver<Db> {
    async fn into_callback_per_message(
        self,
        callback: Box<dyn Fn(Message) + Send + Sync>,
    ) {
        // TODO: unwraps
        self.client
            .sync_once(SyncSettings::default())
            .await
            .unwrap();

        // TODO: take a Sender instead of a callback
        // Explain: the closure below is fine to be a Fn if it TAKES ownership
        // of callback - the problem is when it GIVES AWAY ownership by moving
        // it into the future.

        self.client.add_event_handler(
            move |event: OriginalSyncRoomMessageEvent, room: Room| {
                if let (Room::Joined(room), MessageType::Text(text_content)) =
                    (room, event.content.msgtype)
                {
                    let msg = Message::new(
                        text_content.body,
                        RoomAddress::Matrix(room.room_id().to_string()),
                    );
                    callback(msg);
                }

                std::future::ready(())
            },
        );

        loop {
            let sync_start = Instant::now();
            let sync_result = self.client.sync(SyncSettings::default()).await;
            if let Err(e) = sync_result {
                // TODO: surface error
                println!("Sync returned with error: {e}");

                // Sleep until a reasonable time since the last sync started
                let since_last_sync = sync_start.elapsed();
                if since_last_sync < MIN_TIME_BETWEEN_SYNCS {
                    let sleep_time = MIN_TIME_BETWEEN_SYNCS - since_last_sync;
                    // TODO: surface message
                    println!(
                        "Waiting for a {}s gap between syncs.",
                        MIN_TIME_BETWEEN_SYNCS.as_secs()
                    );
                    sleep(sleep_time).await;
                }
            }
            // sync will never return Ok, so no else.
        }
    }
}
