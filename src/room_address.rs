#[derive(Clone, Debug, PartialEq)]
pub enum RoomAddress {
    Admin,
    CommandLine,
    Matrix(String), // room_id
}
