use async_trait::async_trait;
use std::fmt::Display;
use std::io;

use crate::message::Message;

/**
 * Something that can send messages.
 */
#[async_trait]
pub trait MessageSender {
    async fn send(&mut self, message: &Message) -> Result<(), SendError>;
}

#[derive(Debug)]
pub struct SendError {
    error: Box<dyn std::error::Error>,
}

impl Display for SendError {
    fn fmt(
        &self,
        formatter: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        write!(formatter, "{}", self.error)
    }
}

impl SendError {
    pub fn new(error: Box<dyn std::error::Error>) -> Self {
        Self { error }
    }
}

impl From<io::Error> for SendError {
    fn from(error: io::Error) -> Self {
        Self {
            error: Box::new(error),
        }
    }
}

#[derive(Debug)]
pub struct WrongRoomAddressType {}

impl Display for WrongRoomAddressType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Wrong address type")
    }
}

impl std::error::Error for WrongRoomAddressType {}
