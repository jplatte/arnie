use async_trait::async_trait;
use redis::RedisResult;

use crate::message::Message;

#[async_trait]
pub trait Plugin {
    async fn message(
        &mut self,
        message: &Message,
    ) -> RedisResult<Option<Message>>;
}
