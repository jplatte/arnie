use async_trait::async_trait;
use rand::rngs::SmallRng;
use rand::Rng;
use rand::SeedableRng;
use redis::RedisResult;

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::message::Message;
use crate::plugins::plugin::Plugin;

const SCHEMA_VERSION: usize = 1;
const QUOTES_KEY: &str = "quotes";

const DEFAULT_QUOTES: [&str; 29] = [
    "I need a vacation.",
    "Let off some steam, Bennett.",
    "You’ve just been erased.",
    "Get your ass to Mars!",
    "It's not a toomah!",
    "No problemo.",
    "Screw you!",
    "Chill out, d___wad.",
    "Come with me if you want to live.",
    "If it bleeds, we can kill it.",
    "Do it. Do it now!",
    "See you at the party, Richter!",
    "Get to da choppa!",
    "Hasta la vista, baby.",
    "Consider that a divorce.",
    "I'll be back.",
    "To crush your enemies, see dem driven before you, and to hear de \
    lamentation of der women.",
    "Remember when I said I'd kill you last?  I lied.",
    "You should not drink, and bake.",
    "CROM",
    "You got what you want Cohagen, give dese people air!",
    "Don't disturb my friend, he's dead tired.",
    "You blew my cover!",
    "I am not Quaid!",
    "You think this is the real Quaid? It is!",
    "Sleazy...Demure.",
    "You're Luggage.",
    "Mimetic polyalloy",
    "Sarah Connor?",
];

pub struct ArniePlugin<Db: Database> {
    db: DatabaseWrapper<Db>,
    rng: SmallRng,
}

impl<Db: Database> ArniePlugin<Db> {
    pub async fn new(
        mut db: DatabaseWrapper<Db>,
        random_seed: u64,
    ) -> RedisResult<Self> {
        if !db.exists("installed").await? {
            db.set_usize("installed", SCHEMA_VERSION).await?;
            for quote in DEFAULT_QUOTES {
                db.lpush(QUOTES_KEY, quote).await?;
            }
        }

        Ok(Self {
            db,
            rng: SmallRng::seed_from_u64(random_seed),
        })
    }

    async fn find_quote(&mut self) -> String {
        // TODO: cache quotes
        let num_quotes = self
            .db
            .llen(QUOTES_KEY)
            .await
            .expect("Failed to find quotes!");
        let index = self.rng.gen_range(0..num_quotes);

        self.db
            .lindex(
                QUOTES_KEY,
                isize::try_from(index)
                    .expect("Quote index was unexpectedly large!"),
            ) // TODO: make quotes a const
            .await
            .expect("Failed to get quote!")
            .expect("Quote is missing")
    }

    /// Deletes the quote with index index. (Note: also deletes any other
    /// quotes with value '__deleted__'.)
    pub async fn delete_quote(&mut self, index: isize) -> RedisResult<()> {
        self.db.lset(QUOTES_KEY, index, "__deleted__").await?;
        self.db.lrem(QUOTES_KEY, 0, "__deleted__").await
    }
}

#[async_trait]
impl<Db: Database + Send> Plugin for ArniePlugin<Db> {
    async fn message(
        &mut self,
        message: &Message,
    ) -> RedisResult<Option<Message>> {
        if !message.text.starts_with("!arnie") {
            return Ok(None);
        }

        let quote = if message.text.trim().len() > "!arnie ".len() {
            String::from(&message.text["!arnie ".len()..])
        } else {
            self.find_quote().await
        };

        Ok(Some(Message::new(quote, message.room_address.clone())))
    }
}

#[cfg(test)]
mod test {
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use crate::{memory_database::MemoryDatabase, room_address::RoomAddress};

    use super::*;

    #[tokio::test]
    async fn arnie_says_different_things_every_time() {
        // Given an Arnie plugin
        let db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        );
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // When I ask for two messages, they are different
        assert_eq!(
            &quote_response(&mut arnie).await,
            "Consider that a divorce."
        );

        assert_eq!(
            &quote_response(&mut arnie).await,
            "You think this is the real Quaid? It is!"
        );
    }

    #[tokio::test]
    async fn arnie_doesnt_repopulate_his_quotes_if_already_installed() {
        let underlying_db = Arc::new(Mutex::new(MemoryDatabase::new()));
        let db = DatabaseWrapper::new(
            underlying_db.clone(),
            String::from("test_prefix"),
        );

        // Install arnie into a database
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // Delete all his quotes except the first one
        for i in (1..DEFAULT_QUOTES.len()).rev() {
            arnie.delete_quote(i as isize).await.unwrap();
        }

        // Sanity: he now only ever says one thing
        assert_eq!(&quote_response(&mut arnie).await, "I need a vacation.");
        assert_eq!(&quote_response(&mut arnie).await, "I need a vacation.");
        assert_eq!(&quote_response(&mut arnie).await, "I need a vacation.");

        // Create another instance of arnie on the same same database
        let db =
            DatabaseWrapper::new(underlying_db, String::from("test_prefix"));
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // Check that he didn't recreate all the quotes
        assert_eq!(&quote_response(&mut arnie).await, "I need a vacation.");
        assert_eq!(&quote_response(&mut arnie).await, "I need a vacation.");
        assert_eq!(&quote_response(&mut arnie).await, "I need a vacation.");
    }

    async fn quote_response(arnie: &mut ArniePlugin<MemoryDatabase>) -> String {
        arnie
            .message(&Message::new(
                "!arnie".to_owned(),
                RoomAddress::CommandLine,
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }
}
