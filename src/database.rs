use async_trait::async_trait;
use redis::RedisResult;

#[async_trait]
pub trait Database {
    async fn get(&mut self, key: &str) -> RedisResult<Option<String>>;
    async fn exists(&mut self, key: &str) -> RedisResult<bool>;
    async fn set(&mut self, key: &str, value: &str) -> RedisResult<()>;
    async fn set_usize(&mut self, key: &str, value: usize) -> RedisResult<()>;
    async fn lpush(&mut self, key: &str, value: &str) -> RedisResult<()>;
    /// Delete count elements from the list at key, or all if count == 0.
    /// If count < 0, delete backwards from the end of list.
    async fn lrem(
        &mut self,
        key: &str,
        count: isize,
        value: &str,
    ) -> RedisResult<()>;
    async fn lset(
        &mut self,
        key: &str,
        index: isize,
        value: &str,
    ) -> RedisResult<()>;
    async fn llen(&mut self, key: &str) -> RedisResult<usize>;
    async fn lindex(
        &mut self,
        key: &str,
        index: isize,
    ) -> RedisResult<Option<String>>;
}
